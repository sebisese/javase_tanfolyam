package allatPackage;

public class MainClass {

    public static void main(String[] args) {
        IAllat[] allatok = new IAllat[3];

        allatok[0] = new Husevo();
        allatok[1] = new Novenyevo();
        allatok[2] = new Husevo();

        for (IAllat allat : allatok) {
            allat.eszik();
        }
    }

}
