package szemelyPackage;

public class MainClass {

    public static void main(String[] args) {

        Diak bela = new Diak("6.a", "kovács Béla", 180, 80);
        Tanar andras = new Tanar(new String[]{"5.b", "9.a"}, "Arany András", 175, 90);

        IIskola[] iskolai = {bela, andras};

        for (IIskola i : iskolai) {
//            System.out.println(i);
            System.out.println(i.getOsztaly());
        }

    }

}
