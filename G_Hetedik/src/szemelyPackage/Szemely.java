package szemelyPackage;

public class Szemely {

    protected String nev;
    protected int magassag;
    protected int tomeg;

    public Szemely(String nev, int magassag, int tomeg) {
        this.nev = nev;
        this.magassag = magassag;
        this.tomeg = tomeg;
    }

    @Override
    public String toString() {
        return "Személy{" + "név=" + nev + ", magasság=" + magassag + ", tömeg=" + tomeg + '}';
    }

}
