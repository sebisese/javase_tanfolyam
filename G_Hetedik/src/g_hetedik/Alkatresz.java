package g_hetedik;

public class Alkatresz {

    private String gyarto;
    private String sorozatszam;
    private String leiras;
    private int ar;

    public Alkatresz(String gyarto, String sorozatszam, String leiras, int ar) {
        this.gyarto = gyarto;
        this.sorozatszam = sorozatszam;
        this.leiras = leiras;
        this.ar = ar;
    }

    public String getGyarto() {
        return gyarto;
    }

    public String getSorozatszam() {
        return sorozatszam;
    }

    public String getLeiras() {
        return leiras;
    }

    public int getAr() {
        return ar;
    }

    public void setGyarto(String gyarto) {
        this.gyarto = gyarto;
    }

    public void setSorozatszam(String sorozatszam) {
        this.sorozatszam = sorozatszam;
    }

    public void setLeiras(String leiras) {
        this.leiras = leiras;
    }

    public void setAr(int ar) {
        this.ar = ar;
    }

//    @Override
//    public String toString() {
//        return gyarto + " (" + sorozatszam + ") - " + leiras + ": " + ar;
//    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + ": " + "gyártó=" + gyarto 
                + ", sorozatszám=" + sorozatszam + ", leírás=" + leiras 
                + ", ár=" + ar;
    }
    
    
    

}
