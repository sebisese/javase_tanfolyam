package g_hetedik;

import java.util.StringTokenizer;

public class Szoveg {

    public static void main(String[] args) {

//        int a, b;
//        Integer aa, bb;
//        Integer aaa, bbb;
//
//        a = 2;
//        b = 2;
//
//        aa = 2;
//        bb = 2;
//
//        aaa = new Integer(2);
//        bbb = new Integer(2);
//
//        System.out.println(a == b);
//        System.out.println(aa == bb);
//        System.out.println(aaa == bbb);
//        System.out.println(aaa.equals(bbb));
//        String a = "Hello World!";
//        System.out.println(a.indexOf("z"));
//        String a = "körte";
//        String b = "alma";
//        String c = "barack";
//        String d = "citrom";
//        String e = "cica";
//        
//        System.out.println(a.compareTo(b));
//        System.out.println(b.compareTo(c));
//        System.out.println(c.compareTo(b));
//        System.out.println(b.compareTo(d));
//        System.out.println(d.compareTo(b));
//        System.out.println(d.compareTo(e));
//        
//        String aa = "Ubul";
//        String bb = "ubul";
//        System.out.println(bb.compareTo(aa));
//        System.out.println(bb.compareToIgnoreCase(aa));
//        StringBuilder sb;
//        sb = new StringBuilder();
//        sb.append("süt");
//        sb.append(" a nap");
//        System.out.println(sb);
//        sb.insert(0, "szépen ");
//        System.out.println(sb);
//        sb.delete(4, 10);
//        System.out.println(sb);
        String sz = "Sokan azt hiszik, gondolkodnak, ";
        sz += "pedig csak újrarendezik előítéleteiket.";

        System.out.println("Szóközzel elválasztva:");
        StringTokenizer st = new StringTokenizer(sz);
        while (st.hasMoreElements()) {
            System.out.println(st.nextElement());
        }
        System.out.println("");

        System.out.println("Vesszővel elválasztva:");
        StringTokenizer st2 = new StringTokenizer(sz, ",");
        while (st2.hasMoreElements()) {
            System.out.println(st2.nextElement());
        }

    }

}
