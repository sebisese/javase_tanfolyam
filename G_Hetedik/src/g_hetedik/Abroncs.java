package g_hetedik;

public class Abroncs extends Alkatresz {

    public enum Tipus {
        TELI, NYARI, NEGYEVSZAK
    }
    private Tipus tipus;
    private int kerekmeret;

    public Abroncs(Tipus tipus, int kerekmeret, String gyarto, String sorozatszam, String leiras, int ar) {
        super(gyarto, sorozatszam, leiras, ar);
        this.tipus = tipus;
        this.kerekmeret = kerekmeret;
    }

    public Tipus getTipus() {
        return tipus;
    }

    public int getKerekmeret() {
        return kerekmeret;
    }

    public void setTipus(Tipus tipus) {
        this.tipus = tipus;
    }

    public void setKerekmeret(int kerekmeret) {
        this.kerekmeret = kerekmeret;
    }

//    @Override
//    public String toString() {
//        return super.getGyarto() + " (" + super.getSorozatszam() + ") - "
//                + super.getLeiras() + ", " + tipus + " (" + kerekmeret + "): "
//                + super.getAr();
//    }
    @Override
    public String toString() {
        return super.toString() + ", " + "tipus=" + tipus + ", kerekmeret="
                + kerekmeret;
    }

}
