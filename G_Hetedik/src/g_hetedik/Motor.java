package g_hetedik;

public class Motor extends Alkatresz {

    public enum Tipus {
        BENZIN, DIZEL
    }
    private Tipus tipus;
    private int hengerurtartalom;
    private int teljesitmeny;

    public Motor(Tipus tipus, int hengerurtartalom, int teljesitmeny, String gyarto, String sorozatszam, String leiras, int ar) {
        super(gyarto, sorozatszam, leiras, ar);
        this.tipus = tipus;
        this.hengerurtartalom = hengerurtartalom;
        this.teljesitmeny = teljesitmeny;
    }

    public Tipus getTipus() {
        return tipus;
    }

    public int getHengerurtartalom() {
        return hengerurtartalom;
    }

    public int getTeljesitmeny() {
        return teljesitmeny;
    }

    public void setTipus(Tipus tipus) {
        this.tipus = tipus;
    }

    public void setHengerurtartalom(int hengerurtartalom) {
        this.hengerurtartalom = hengerurtartalom;
    }

    public void setTeljesitmeny(int teljesitmeny) {
        this.teljesitmeny = teljesitmeny;
    }

//    @Override
//    public String toString() {
//        return super.getGyarto() + " (" + super.getSorozatszam() + ") - "
//                + super.getLeiras() + ", " + tipus + " (" + hengerurtartalom
//                + ", " + teljesitmeny + "): " + super.getAr();
//    }

    @Override
    public String toString() {
        return super.toString() + ", " + "tipus=" + tipus 
                + ", hengerurtartalom=" + hengerurtartalom + ", teljesitmeny=" 
                + teljesitmeny;
    }
    
    

}
