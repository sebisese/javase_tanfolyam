package g_hetedik;

public class G_Hetedik {

    public static void main(String[] args) {
        
        Alkatresz alk = new Alkatresz("Audi", "AAA123456", "sebváltó", 42000);
        System.out.println(alk);
        
        Abroncs abr = new Abroncs(Abroncs.Tipus.NYARI, 18, "Pirelli", "P123", "abroncs", 29000);
        System.out.println(abr);
        
        Motor mtr = new Motor(Motor.Tipus.BENZIN, 4000, 13, "BMW", "BW999", "nagy motor", 80000);
        System.out.println(mtr);
        
        System.out.println("***");
        
        Alkatresz[] alkatreszek = {alk, abr, new Abroncs(Abroncs.Tipus.TELI, 16, "Pirelli", "PI77", "téli", 22000)};
        for (Alkatresz alkatresz : alkatreszek) {
            System.out.println(alkatresz);
        }
        
    }
    
}
