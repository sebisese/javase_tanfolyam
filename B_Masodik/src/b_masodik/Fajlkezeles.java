package b_masodik;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Fajlkezeles {
    
    public static void main(String[] args) throws IOException {
        String [] gyumolcsok = {"alma", "körte", "barack"};
        PrintWriter pw = new PrintWriter(new FileWriter("gyumolcs.txt"));
        for (String egyGyumolcs : gyumolcsok) {
            pw.println(egyGyumolcs);
        }
        pw.close();
        
        BufferedReader br = new BufferedReader(new FileReader("gyumolcs.txt"));
        String sor;
        while ((sor = br.readLine()) != null) {
            System.out.println(sor);
        }
        br.close();
    }
    
}
