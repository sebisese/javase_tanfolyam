package b_masodik;

import java.util.Random;
import java.util.Scanner;

public class C {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        //Elágazás / 4
//        System.out.println("Mennyit kerestél?");
//        int kereset = in.nextInt();
//
//        int kolt;
//        if ((kolt = kereset % 5000) != 0) {
//            System.out.println("Elkölthetsz " + kolt + " Forintot");
//        }
        
        
        //Elágazás / 9
        int a = in.nextInt();
        int b = in.nextInt();
        int c = in.nextInt();
        
        if ( a + b > c || a + c > b || b + c > a) {
            System.out.println("Megszerkeszthető.");
            double g = (Math.sqrt(a) + Math.sqrt(b) - Math.sqrt(c)) / (2 * a * b);
        } else {
            System.out.println("Nem szerkesztehtő meg.");
        }
        
        
        //Ciklus / 4
//        int a = in.nextInt();
//        int b = in.nextInt();
//        int szorzat = 0;
//        for (int i = 1; i <= a; i++) {
//            szorzat += b;
//        }
//        System.out.println(szorzat);
        
        //Ciklus / 5
//        int a = in.nextInt();
//        int b = in.nextInt();
//        int hatvany = a;
//        for (int i = 1; i < b; i++) {
//            hatvany *= a;
//        }
//        System.out.println(hatvany);
        
        
        //Ciklus / 6
//        int a = in.nextInt();
//        int b = in.nextInt();
//        
//        if (a < b) {
//            int t = a;
//            a = b;
//            b = t;
//        }
//        
//        int maradek = a % b;
//        while (maradek > 0) {
//            a = b;
//            b = maradek;
//            maradek = a % b;
//        }
//        System.out.println(b);
        
        
        //Ciklus / 7 v1
//        int a = in.nextInt();
//        int b = in.nextInt();
//        
//        int tobbszoros = a * b;
//        
//        if (a < b) {
//            int t = a;
//            a = b;
//            b = t;
//        }
//        
//        int maradek = a % b;
//        while (maradek > 0) {
//            a = b;
//            b = maradek;
//            maradek = a % b;
//        }
//        System.out.println(tobbszoros / b);
        
        //Ciklus / 7 v2
//        int a = in.nextInt();
//        int b = in.nextInt();
//        int x = a;
//        int y = b;
//        while (x != y) {
//            if (x < y) {
//                x = x + a;
//            } else if (x > y) {
//               y = y + b; 
//            }
//        }
//        System.out.println(x);
        
        
        //Ciklus / 8
//        Random rnd = new Random();
//        int szam = rnd.nextInt(101) - 1;
//        int i = 0;
//        System.out.println("Gondoltam egy számra 1 és 100 között (" + szam + ").");
//        do {
//            System.out.print("Add meg a számot: ");
//            i = in.nextInt();
//        } while ( i != szam);
//        System.out.println("Eltaláltad!");
        
    }
}
