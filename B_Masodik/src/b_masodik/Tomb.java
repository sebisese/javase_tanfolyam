package b_masodik;

public class Tomb {

    public static void main(String[] args) {

        //tömb
//        int[] tomb = {21, 23, 56, 87};
//        for (int i = 0; i < tomb.length; i++) {
//            System.out.println(tomb[i]);
//        }
//        System.out.println("**********************");
//        for (int elem : tomb) {
//            System.out.println(elem);
//        }
        
        //mátrix
        int[][] m = {{1, 22, 300, 4},
                     {5, 6, 777, 8}};
        for (int[] m1 : m) {
            for (int n : m1) {
                System.out.print(n + "\t");
            }
            System.out.println();
        }

    }
}
