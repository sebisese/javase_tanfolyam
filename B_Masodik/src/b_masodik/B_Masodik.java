package b_masodik;

public class B_Masodik {

    public static void main(String[] args) {
        int i = 1;
        do {
            System.out.println(i);
            i++;
        } while (i <= 10);
        
        System.out.println("----------");
        
        int j = 1;
        while (j <= 10) {
            System.out.println(j);
            j++;
        }
        
        System.out.println("----------");
        
        for (int k = 1; k <= 10; k++) {
            System.out.println(k);
        }
    }

}
