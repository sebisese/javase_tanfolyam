package papirgyujtes;

import java.util.ArrayList;
import java.util.List;

public class Iskola {

    private List<Tanulo> tanulok;

    public Iskola(List<Tanulo> tanulok) {
        this.tanulok = tanulok;
    }

    public Iskola() {
        tanulok = new ArrayList<>();
    }

    public void felvesz(Tanulo t) {
        tanulok.add(t);
    }

    public void kirug(Tanulo t) {
        tanulok.remove(t);
    }

    public void kirug(String nev) {
        tanulok.remove(new Tanulo(nev));
    }

    public Integer papirosszeg() {
        int osszeg = 0;
        for (Tanulo t : tanulok) {
            osszeg += t.getTomeg();
        }
        return osszeg;
    }
    
    public Tanulo legtobbPapirtHozta() {
        int max = 0;
        for (int i = 1; i < tanulok.size(); i++) {
            if (tanulok.get(max).getTomeg() < tanulok.get(i).getTomeg()) {
                max = i;
            }
        }
        return tanulok.get(max);
    }
    
    public double legtobbetHozoSzazaleka() {
        return (double)legtobbPapirtHozta().getTomeg() / (double)papirosszeg() * 100;
    }

    @Override
    public String toString() {
        return "Iskola{" + "tanulok=" + tanulok + '}';
    }

}
