package papirgyujtes;

import java.util.Objects;

public class Tanulo {

    private String nev;
    private Integer tomeg;

    public Tanulo(String nev, Integer tomeg) {
        this.nev = nev;
        this.tomeg = tomeg;
    }

    public Tanulo(String nev) {
        this.nev = nev;
    }

    public Integer getTomeg() {
        return tomeg;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Tanulo other = (Tanulo) obj;
        if (!Objects.equals(this.nev, other.nev)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Tanulo{" + "nev=" + nev + ", tomeg=" + tomeg + '}';
    }

}
