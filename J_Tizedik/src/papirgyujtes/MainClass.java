package papirgyujtes;

public class MainClass {

    public static void main(String[] args) {

        Iskola iskola = new Iskola();
        iskola.felvesz(new Tanulo("Kovács Béla", 1));
        iskola.felvesz(new Tanulo("Tóth András", 50));
        iskola.felvesz(new Tanulo("Szabó Eszter", 12));

        System.out.println("Összes papír: " + iskola.papirosszeg());
        System.out.println("Legtöbbet hozta: " + iskola.legtobbPapirtHozta());
        
        iskola.kirug("Tóth András");

        System.out.println("Tanulók: " + iskola);
        System.out.println("összes papír: " + iskola.papirosszeg());        
        System.out.println("Legtöbbet hozta: " + iskola.legtobbPapirtHozta());        
        System.out.println("Legtöbbet hozott százaléka: " + iskola.legtobbetHozoSzazaleka());

    }

}
