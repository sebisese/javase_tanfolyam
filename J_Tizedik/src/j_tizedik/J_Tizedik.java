package j_tizedik;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;

public class J_Tizedik {

    public static void main(String[] args) {
        
        HashSet<Integer> a = new HashSet<>(Arrays.asList(1,2,3,4));
        System.out.print("a: ");
        System.out.println(a);
        
        HashSet<Integer> b = new HashSet<>(Arrays.asList(3,4,5,6));
        System.out.print("b: ");
        System.out.println(b);
        
        HashSet<Integer> c = kulonbseg(a, b);
        System.out.print("Különbség: ");
        System.out.println(c);

        HashSet<Integer> d = unio(a, b);
        System.out.print("Unió: ");
        System.out.println(d);
    }

    private static HashSet kulonbseg(HashSet a, HashSet b) {
        a.removeAll(b);
        return a;
//        HashSet c = new HashSet();
//        Iterator it = a.iterator();
//        while (it.hasNext()) {
//            Object o = it.next();
//            if (!b.contains(o)) {
//                c.add(o);
//            }
//        }
//        return c;
    }

    private static HashSet unio(HashSet a, HashSet b) {
        a.addAll(b);
        return a;
//        HashSet c = b;
//        Iterator it = a.iterator();
//        while (it.hasNext()) {
//            Object o = it.next();
//            if (!b.contains(o)) {
//                c.add(o);
//            }
//        }
//        return c;
    }

}
