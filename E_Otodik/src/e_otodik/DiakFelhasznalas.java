package e_otodik;

public class DiakFelhasznalas {

    public static void main(String[] args) {
        
        Diak d = new Diak("Dénes", 175, 70);
        Diak e = new Diak("Elemér", 195, 80);
        Diak f = new Diak("Ferenc", 165, 90);
        
        Diak[] diakok = {d, e, f};
        
        for (int i = 0; i < Diak.diakokSzama(); i++) {
            System.out.println(diakok[i]);
            System.out.println("---");
        }
        System.out.println("Diákok száma: " + Diak.diakokSzama());
        
    }
}
