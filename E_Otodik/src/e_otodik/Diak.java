package e_otodik;

public class Diak {

    private String nev;
    private double testmagassag;
    private double testsuly;
    private static int diakokSzama = 0;

    public Diak(String nev, double testmagassag, double testsuly) {
        this.nev = nev;
        this.testmagassag = testmagassag;
        this.testsuly = testsuly;
        diakokSzama++;
    }

    public static int diakokSzama() {
        return diakokSzama;
    }

    public double testtomegindex() {
        return testsuly / Math.pow(testmagassag/100, 2);
    }

    @Override
    public String toString() {
        String s = "";
        s += "Név: " + nev + "\n";
        s += "Magassag: " + testmagassag + "\n";
        s += "Súly: " + testsuly + "\n";
        s += "Testtömegindex: " + testtomegindex();
        return s;
    }

}
