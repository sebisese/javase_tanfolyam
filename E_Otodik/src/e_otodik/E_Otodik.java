package e_otodik;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class E_Otodik {

    static int[] diakok;

    public static void main(String[] args) throws IOException {

        beolvas("diak.csv");

        System.out.println("170 cm-nél magasabb diákok száma: " + magasDiak(diakok));

        System.out.println("Átlagmagasság: " + atlagmagassag(diakok));
        System.out.print("Átlagmagasság fölötti magasságok: ");
        tombKiir(atlagnalMagasabbak(diakok), diakok);

        if (tornasorbanVannakE(diakok)) {
            System.out.println("Tornasorban vannak.");
        } else {
            System.out.println("Nincsenek tornasorban.");
        }

    }

    private static void beolvas(String fajl) throws IOException {
        String egeszSzoveg = new String(Files.readAllBytes(Paths.get(fajl)));
        diakok = new int[egeszSzoveg.split(";").length];
        for (int i = 0; i < diakok.length; i++) {
            diakok[i] = Integer.parseInt(egeszSzoveg.split(";")[i]);
        }
    }

    private static int magasDiak(int[] diakok) {
        int c = 0;
        for (int e : diakok) {
            if (e > 170) {
                c++;
            }
        }
        return c;
    }

    private static double atlagmagassag(int[] diakok) {
        double sum = 0;
        for (int e : diakok) {
            sum += e;
        }
        return sum / diakok.length;
    }

    private static int[] atlagnalMagasabbak(int[] diakok) {
        int[] magasak = new int[diakok.length];
        double atlag = atlagmagassag(diakok);
        int db = 0;
        for (int i = 0; i < diakok.length; i++) {
            if (diakok[i] > atlag) {
                magasak[db] = i;
                db++;
            }
        }
        return java.util.Arrays.copyOf(magasak, db);
    }

    private static void tombKiir(int[] atlagnalMagasabbak, int[] diakok) {
        for (int e : atlagnalMagasabbak) {
            System.out.print(diakok[e] + " ");
        }
        System.out.print("\n");
    }

    private static boolean tornasorbanVannakE(int[] diakok) {
        int i = 0;
        while (i < diakok.length - 1 && (diakok[i] > diakok[i+1])) {
            i++;
        }
        return i == diakok.length - 1;
    }

}
