package e_otodik;

public class Auto2 {

    private static int literenkentiAr = 420;
    private String rendszam;
    private int tartalyKapacitas;
    private double tartalySzint;
    private double fogyasztas;

    public Auto2(String rendszam, int tartalyKapacitas, double tartalySzint, double fogyasztas) {
        this.rendszam = rendszam;
        this.tartalyKapacitas = tartalyKapacitas;
        this.tartalySzint = tartalySzint;
        this.fogyasztas = fogyasztas;
    }

    public double hanyLitertFogFogyasztani(int km) {
        return fogyasztas / 100 * km;
    }

    public double mennyibeKerult(int km) {
        return literenkentiAr * hanyLitertFogFogyasztani(km);
    }

    public static void benzinarValtozas(int ujAr) {
        literenkentiAr = ujAr;
    }

}
