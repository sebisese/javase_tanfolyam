package e_otodik;

public class Auto {

    private String rendszam;
    private int tartalyKapacitas;
    private double tartalySzint;
    private double fogyasztas;

    public Auto(String rendszam, int tartalyKapacitas, double tartalySzint, double fogyasztas) {
        this.rendszam = rendszam;
        this.tartalyKapacitas = tartalyKapacitas;
        this.tartalySzint = tartalySzint;
        this.fogyasztas = fogyasztas;
    }
    
    public double hanyLitertFogFogyasztani(int km) {
        return fogyasztas / 100 * km;
    }

}
