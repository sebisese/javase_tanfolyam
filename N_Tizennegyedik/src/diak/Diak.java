package diak;

public class Diak {

    Integer magassag;
    String nev;

    public Diak(Integer magassag, String nev) {
        this.magassag = magassag;
        this.nev = nev;
    }

    @Override
    public String toString() {
        return nev;
    }

    public Integer getMagassag() {
        return magassag;
    }

    public String getNev() {
        return nev;
    }

    public void setMagassag(Integer magassag) {
        this.magassag = magassag;
    }

    public void setNev(String nev) {
        this.nev = nev;
    }

}
