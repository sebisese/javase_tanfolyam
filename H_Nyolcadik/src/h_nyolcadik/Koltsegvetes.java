package h_nyolcadik;

import java.util.Date;

public class Koltsegvetes {

    private Date datum;
    private String megnevezes;
    private double osszeg;

    public Koltsegvetes(Date datum, String megnevezes, double osszeg) {
        this.datum = datum;
        this.megnevezes = megnevezes;
        this.osszeg = osszeg;
    }

    public Date getDatum() {
        return datum;
    }

    public String getMegnevezes() {
        return megnevezes;
    }

    public double getOsszeg() {
        return osszeg;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public void setMegnevezes(String megnevezes) {
        this.megnevezes = megnevezes;
    }

    public void setOsszeg(double osszeg) {
        this.osszeg = osszeg;
    }

    @Override
    public String toString() {
        return "Koltsegvetes{" + "datum=" + datum + ", megnevezes=" + megnevezes + ", osszeg=" + osszeg + '}';
    }
    
}
