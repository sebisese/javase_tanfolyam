package h_nyolcadik;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

public class Excel {

    public static void main(String[] args) throws FileNotFoundException, IOException {

        FileInputStream file = new FileInputStream(new File("Koltsegvetes.xls"));
        HSSFWorkbook workbook = new HSSFWorkbook(file);
        HSSFSheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.iterator();

        Koltsegvetes[] koltsegvetes = new Koltsegvetes[1000];

        //az első 4 sorra nincs szükségünk
        for (int j = 0; j < 4; j++) {
            rowIterator.next();
        }
            
        int i = 0;
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            koltsegvetes[i] = new Koltsegvetes(row.getCell(0).getDateCellValue(), 
                    row.getCell(1).getStringCellValue(), 
                    row.getCell(2).getNumericCellValue());
            i++;
        }
        koltsegvetes = java.util.Arrays.copyOf(koltsegvetes, i);
        
        for (Koltsegvetes koltsegvete : koltsegvetes) {
            System.out.println(koltsegvete);
        }

    }

}
