package h_nyolcadik;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Fajlmuveletek {

    public static void main(String[] args) throws IOException {

        String p = new File(".").getCanonicalPath();
        Path path = Paths.get(p);

        konyvtarBejaro(path, 0);
    }

    private static void konyvtarBejaro(Path path, int szint) {
        try {
            DirectoryStream<Path> stream = Files.newDirectoryStream(path);
            for (Path file : stream) {
                for (int i = 0; i < szint; i++) {
                    System.out.print("  ");
                }
                System.out.println(file.getFileName());
                if (Files.isDirectory(file)) {
                    konyvtarBejaro(file, szint + 1);
                }
            }
        } catch (IOException | DirectoryIteratorException x) {
            System.err.println(x);
        }
    }

}
