package szemelyPackage;

public class Tanar extends Szemely implements IIskola {

    protected String osztalyok[];

    public Tanar(String[] osztalyok, String nev, int magassag, int tomeg) {
        super(nev, magassag, tomeg);
        this.osztalyok = osztalyok;
    }

    @Override
    public String getOsztaly() {
        String s = "";
        for (String osztaly : osztalyok) {
            s += osztaly + " ";
        }
        return s;
    }

    @Override
    public String toString() {
        return super.toString() + " és Tanár{" + "osztályok=" + getOsztaly() + '}';
    }

}
