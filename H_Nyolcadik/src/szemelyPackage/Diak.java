package szemelyPackage;

import java.io.Serializable;

public class Diak extends Szemely implements IIskola, Serializable {

    protected String osztaly;

    public Diak(String osztaly, String nev, int magassag, int tomeg) {
        super(nev, magassag, tomeg);
        this.osztaly = osztaly;
    }

    @Override
    public String getOsztaly() {
        return osztaly;
    }

    @Override
    public String toString() {
        return super.toString() + " és Diák{" + "osztály=" + osztaly + '}';
    }

}
