package szemelyPackage;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Fajlbairas {

    public static void main(String[] args) throws FileNotFoundException, IOException {

        Diak bela = new Diak("6.a", "Kovács Béla", 180, 80);
        Diak andras = new Diak("5.b", "Arany András", 175, 90);
        Diak[] diakok = {bela, andras};

        /**
         * **************************** 1 ************************************
         */
        //kiírás
        DataOutputStream out = new DataOutputStream(
                new BufferedOutputStream(
                        new FileOutputStream("diakok.dat")));

        for (int i = 0; i < diakok.length; i++) {
            out.writeUTF(diakok[i].getNev());
            out.writeInt(diakok[i].getMagassag());
        }
        out.close();

        //beolvasás
        DataInputStream in = new DataInputStream(
                new BufferedInputStream(
                        new FileInputStream("diakok.dat")));
        String nev;
        int magassag;
        try {
            while (true) {
                nev = in.readUTF();
                magassag = in.readInt();
                System.out.println(nev + " " + magassag);
            }
        } catch (EOFException e) {
            System.out.println("***Vége a fájlnak.");
        }

        /**
         * **************************** 2 ************************************
         */
        //kiírás
        ObjectOutputStream out2 = new ObjectOutputStream(
                new FileOutputStream("diakok2.dat"));
        for (int i = 0; i < diakok.length; i++) {
            out2.writeObject(diakok[i]);
        }
        out2.close();

        //beolvasás
        Diak[] osztaly = new Diak[1000];
        ObjectInputStream in2 = new ObjectInputStream(
                new FileInputStream("diakok2.dat"));
        int i = 0;
        try {
            while (true) {
                osztaly[i] = (Diak) in2.readObject();
                i++;
            }
        } catch (EOFException e) {
            osztaly = java.util.Arrays.copyOf(osztaly, i);
        } catch (ClassNotFoundException ex) {
        }

        for (Diak diak : osztaly) {
            System.out.println(diak);
        }

    }

}
