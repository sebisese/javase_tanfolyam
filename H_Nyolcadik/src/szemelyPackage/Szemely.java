package szemelyPackage;

import java.io.Serializable;

public class Szemely implements Serializable {

    protected String nev;
    protected int magassag;
    protected int tomeg;

    public Szemely(String nev, int magassag, int tomeg) {
        this.nev = nev;
        this.magassag = magassag;
        this.tomeg = tomeg;
    }

    public String getNev() {
        return nev;
    }

    public int getMagassag() {
        return magassag;
    }

    public int getTomeg() {
        return tomeg;
    }

    @Override
    public String toString() {
        return "Személy{" + "név=" + nev + ", magasság=" + magassag + ", tömeg=" + tomeg + '}';
    }

}
