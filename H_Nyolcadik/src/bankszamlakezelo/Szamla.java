package bankszamlakezelo;

public class Szamla {

    protected String szamlaszam;
    protected int osszeg;
    protected static int[] ellenorzo = {9, 7, 3, 1, 9, 7, 3, 1};

    public Szamla(String szamlaszam, int osszeg) throws RosszSzamlaszamException {
        if (!bankszamlaEllenorzo(szamlaszam)) {
            throw new RosszSzamlaszamException();
        }
        this.szamlaszam = szamlaszam;
        this.osszeg = osszeg;
    }

    public Szamla(String szamlaszam) throws RosszSzamlaszamException {
        if (!bankszamlaEllenorzo(szamlaszam)) {
            throw new RosszSzamlaszamException();
        }
        this.szamlaszam = szamlaszam;
        this.osszeg = 0;
    }

    public String getSzamlaszam() {
        return szamlaszam;
    }

    public int getOsszeg() {
        return osszeg;
    }

    public void setSzamlaszam(String szamlaszam) {
        this.szamlaszam = szamlaszam;
    }

    public void setOsszeg(int osszeg) {
        this.osszeg = osszeg;
    }

    private static boolean bankszamlaEllenorzo(String szamlaszam) {
        String darabok[] = szamlaszam.split("-");
        int i = 0;
        while (i < darabok.length && jo(darabok[i])) {
            i++;
        }
        return i == darabok.length;
    }

    private static boolean jo(String nyolcas) {
        int o = 0;
        for (int i = 0; i < 8; i++) {
            o += (nyolcas.charAt(i) - 48) * ellenorzo[i];  //ASCII-ban a számok 48-tól kezdődnek
        }
        return o % 10 == 0;
    }

    @Override
    public String toString() {
        return "Számlaszám: " + szamlaszam + ", számlán található összeg:" + osszeg;
    }
}
