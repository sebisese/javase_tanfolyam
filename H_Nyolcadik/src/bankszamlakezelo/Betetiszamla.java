package bankszamlakezelo;

import java.util.Date;

public class Betetiszamla extends Szamla {

    private Date betetIdopontja;
    private int betetiNapokSzama;
    private static double kamat = 0.02;

    public Betetiszamla(String szamlaszam, int osszeg, int betetiNapokSzama) throws RosszSzamlaszamException {
        super(szamlaszam, osszeg);
        this.betetIdopontja = new Date();
        this.betetiNapokSzama = betetiNapokSzama;
    }

    public Betetiszamla(String szamlaszam, int osszeg, Date betetIdopontja, int betetiNapokSzama) throws RosszSzamlaszamException {
        super(szamlaszam, osszeg);
        this.betetIdopontja = betetIdopontja;
        this.betetiNapokSzama = betetiNapokSzama;
    }
    
//    public static double kifizetes(){
//        
//    }

    @Override
    public String toString() {
        return "Betétiszámla " + super.toString();
    }

}
