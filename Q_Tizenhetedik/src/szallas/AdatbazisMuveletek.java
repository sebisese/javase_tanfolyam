package szallas;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AdatbazisMuveletek {

    private String dbFajl;
    private static String dbSzallasTablanev = "TSzallas";

    public AdatbazisMuveletek() {
        dbFajl = "res/szallas.db";
        if (!isTableExists(dbSzallasTablanev)) {
            createSzallas();
        }
    }

    public AdatbazisMuveletek(String dbFajl) {
        this.dbFajl = dbFajl;
        if (!isTableExists(dbSzallasTablanev)) {
            createSzallas();
        }
    }

    public void insertSzallas(Szallas szallas) {
        String sql = "INSERT INTO " + dbSzallasTablanev + " VALUES (NULL,?,?,?,?,?,?)";
        Connection c = null;
        PreparedStatement s = null;
        try {
            //SQLite:
//            Class.forName("org.sqlite.JDBC");
//            c = DriverManager.getConnection("jdbc:sqlite:" + dbFajl);
            //MySQL:
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            c = DriverManager.getConnection("jdbc:mysql://localhost:3306/szallas?zeroDateTimeBehavior=convertToNull", "bela", "almafa12");
            
            s = c.prepareStatement(sql);
            s.setString(1, szallas.getSzallasNeve());
            s.setInt(2, szallas.getVaros().getId());
            s.setInt(3, szallas.getCsillagokSzama());
            s.setInt(4, szallas.getAr());
            s.setInt(5, szallas.getTavolsagReptertol());
            s.setInt(6, szallas.getTavolsagTengerparttol());
            s.executeUpdate();
            s.close();
            System.out.println("Mentve.");
        } catch (ClassNotFoundException | SQLException | InstantiationException | IllegalAccessException ex) {
            ex.printStackTrace();
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public void updateSzallas(Szallas szallas) {
        String sql = "UPDATE " + dbSzallasTablanev + " SET "
                + "szallasNeve=?, "
                + "varosId=?, "
                + "csillagokSzama=?, "
                + "ar=?, "
                + "tavolsagReptertol=?, "
                + "tavolsagTengerparttol=? "
                + "WHERE id=?";
        Connection c = null;
        PreparedStatement s = null;
        try {
            //SQLite:
//            Class.forName("org.sqlite.JDBC");
//            c = DriverManager.getConnection("jdbc:sqlite:" + dbFajl);
            //MySQL:
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            c = DriverManager.getConnection("jdbc:mysql://localhost:3306/szallas?zeroDateTimeBehavior=convertToNull", "bela", "almafa12");
          
            s = c.prepareStatement(sql);
            s.setString(1, szallas.getSzallasNeve());
            s.setInt(2, szallas.getVaros().getId());
            s.setInt(3, szallas.getCsillagokSzama());
            s.setInt(4, szallas.getAr());
            s.setInt(5, szallas.getTavolsagReptertol());
            s.setInt(6, szallas.getTavolsagTengerparttol());
            s.setInt(7, szallas.getId());
            s.executeUpdate();
            s.close();
            System.out.println("Módosítva.");
        } catch (ClassNotFoundException | SQLException | InstantiationException | IllegalAccessException ex) {
            ex.printStackTrace();
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public List<Varos> getVarosok() {
        Connection c = null;
        Statement s = null;
        String sql = "SELECT * FROM TVaros ORDER BY varosNeve";
        List<Varos> result = new ArrayList<>();
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            c = DriverManager.getConnection("jdbc:mysql://localhost:3306/szallas?zeroDateTimeBehavior=convertToNull", "bela", "almafa12");
            
            s = c.createStatement();
            ResultSet r = s.executeQuery(sql);
            while (r.next()) {
                Varos varos = new Varos();
                varos.setId(r.getInt("id"));
                varos.setOrszagNeve(r.getString("orszagNeve"));
                varos.setVarosNeve(r.getString("varosNeve"));
                result.add(varos);
            }
            s.close();
            System.out.println("Városok lekérdezve.");
        } catch (ClassNotFoundException | SQLException | InstantiationException | IllegalAccessException ex) {
            ex.printStackTrace();
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return result;
    }

    public List<Szallas> getSzallasok() {
        Connection c = null;
        Statement s = null;
        String sql = "SELECT sz.*, v.varosNeve, v.OrszagNeve FROM TSzallas sz INNER JOIN TVaros v ON sz.varosId = v.id";
        List<Szallas> result = new ArrayList<>();
        try {
            //SQLite:
//            Class.forName("org.sqlite.JDBC");
//            c = DriverManager.getConnection("jdbc:sqlite:" + dbFajl);
            //MySQL:
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            c = DriverManager.getConnection("jdbc:mysql://localhost:3306/szallas?zeroDateTimeBehavior=convertToNull", "bela", "almafa12");
            
            s = c.createStatement();
            ResultSet r = s.executeQuery(sql);
            while (r.next()) {
                Szallas szallas = new Szallas();
                szallas.setId(r.getInt("id"));
                szallas.setSzallasNeve(r.getString("szallasNeve"));
                szallas.getVaros().setId(r.getInt("varosId"));
                szallas.getVaros().setOrszagNeve(r.getString("orszagNeve"));
                szallas.getVaros().setVarosNeve(r.getString("varosNeve"));
                szallas.setCsillagokSzama(r.getInt("csillagokSzama"));
                szallas.setAr(r.getInt("ar"));
                szallas.setTavolsagReptertol(r.getInt("tavolsagReptertol"));
                szallas.setTavolsagTengerparttol(r.getInt("tavolsagTengerparttol"));
                result.add(szallas);
            }
            s.close();
            System.out.println("Szállások lekérdezve.");
        } catch (ClassNotFoundException | SQLException | InstantiationException | IllegalAccessException ex) {
            ex.printStackTrace();
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return result;
    }

    public boolean isTableExists(String tablanev) {
        Connection c = null;
        DatabaseMetaData dbm;
        boolean result = false;
        try {
            //SQLite:
//            Class.forName("org.sqlite.JDBC");
//            c = DriverManager.getConnection("jdbc:sqlite:" + dbFajl);
            //MySQL:
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            c = DriverManager.getConnection("jdbc:mysql://localhost:3306/szallas?zeroDateTimeBehavior=convertToNull", "bela", "almafa12");
            
            dbm = c.getMetaData();
            ResultSet tables = dbm.getTables(null, null, tablanev, null);
            if (tables.next()) {
                result = true;
                System.out.println(tablanev + " tábla létezik. Hurrá!");
            } else {
                System.out.println(tablanev + " tábla nem létezik. Jajj, de no para, létrehozom...");
            }
        } catch (ClassNotFoundException | SQLException | InstantiationException | IllegalAccessException ex) {
            ex.printStackTrace();
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return result;
    }

    private void createSzallas() {
        String sqlVaros = "CREATE TABLE TVaros ("
                + "	id INTEGER PRIMARY KEY AUTO_INCREMENT," //MySQL-hez kell az AUTO_INCREMENT
                + "	orszagNeve TEXT NOT NULL,"
                + "	varosNeve TEXT NOT NULL"
                + ")";
        updateFuttat(sqlVaros);
        
        String sql = "CREATE TABLE " + dbSzallasTablanev + " ("
                + "	id INTEGER PRIMARY KEY AUTO_INCREMENT," //MySQL-hez kell az AUTO_INCREMENT
                + "	szallasNeve TEXT NOT NULL,"
                + "	varosId INTEGER NOT NULL,"
                + "	csillagokSzama INTEGER NOT NULL,"
                + "	ar INTEGER NOT NULL,"
                + "	tavolsagReptertol INTEGER NOT NULL,"
                + "	tavolsagTengerparttol INTEGER NOT NULL"
                + ")";
        updateFuttat(sql);
    }

    private void updateFuttat(String sql) {
        Connection c = null;
        Statement s = null;
        try {
            //SQLite:
//            Class.forName("org.sqlite.JDBC");
//            c = DriverManager.getConnection("jdbc:sqlite:" + dbFajl);
            //MySQL:
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            c = DriverManager.getConnection("jdbc:mysql://localhost:3306/szallas?zeroDateTimeBehavior=convertToNull", "bela", "almafa12");
            
            s = c.createStatement();
            s.executeUpdate(sql);
            s.close();
            System.out.println("Lefuttattam: " + sql);
        } catch (ClassNotFoundException | SQLException | InstantiationException | IllegalAccessException ex) {
            ex.printStackTrace();
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
}
