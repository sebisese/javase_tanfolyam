package szallas;

public class Varos {
    
    private int id;
    private String orszagNeve;
    private String varosNeve;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOrszagNeve() {
        return orszagNeve;
    }

    public void setOrszagNeve(String orszagNeve) {
        this.orszagNeve = orszagNeve;
    }

    public String getVarosNeve() {
        return varosNeve;
    }

    public void setVarosNeve(String varosNeve) {
        this.varosNeve = varosNeve;
    }

    @Override
    public String toString() {
        return varosNeve;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Varos other = (Varos) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }
}
