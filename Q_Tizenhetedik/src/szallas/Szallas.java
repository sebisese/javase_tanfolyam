package szallas;

public class Szallas {

    private int id;
    private String szallasNeve;
    private Varos varos;
    private int csillagokSzama;
    private int ar;
    private int tavolsagReptertol;
    private int tavolsagTengerparttol;

    public Szallas() {
        varos = new Varos();
    }

    public int getId() {
        return id;
    }

    public String getSzallasNeve() {
        return szallasNeve;
    }

    public Varos getVaros() {
        return varos;
    }

    public int getCsillagokSzama() {
        return csillagokSzama;
    }

    public int getAr() {
        return ar;
    }

    public int getTavolsagReptertol() {
        return tavolsagReptertol;
    }

    public int getTavolsagTengerparttol() {
        return tavolsagTengerparttol;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setSzallasNeve(String szallasNeve) {
        this.szallasNeve = szallasNeve;
    }

    public void setVaros(Varos varos) {
        this.varos = varos;
    }

    public void setCsillagokSzama(int csillagokSzama) {
        this.csillagokSzama = csillagokSzama;
    }

    public void setAr(int ar) {
        this.ar = ar;
    }

    public void setTavolsagReptertol(int tavolsagReptertol) {
        this.tavolsagReptertol = tavolsagReptertol;
    }

    public void setTavolsagTengerparttol(int tavolsagTengerparttol) {
        this.tavolsagTengerparttol = tavolsagTengerparttol;
    }

    @Override
    public String toString() {
        return szallasNeve;
    }

}
