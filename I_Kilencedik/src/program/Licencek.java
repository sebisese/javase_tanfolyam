package program;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;

public class Licencek implements Serializable {

    private ProgramLicence[] licenceTomb;
    private transient String fajlnev; //transient: nem lesz serializálva

    public Licencek() {
        licenceTomb = new ProgramLicence[0];
    }
    
    public Licencek(String fajlnev) throws IOException, FileNotFoundException, ClassNotFoundException {
        open(fajlnev);
        this.fajlnev = fajlnev;
    }

    public void addLicence(ProgramLicence licence) {
        licenceTomb = Arrays.copyOf(licenceTomb, licenceTomb.length + 1);
        licenceTomb[licenceTomb.length - 1] = licence;
    }

    public void addLicence(String excelFajl) throws IOException {
        FileInputStream file = new FileInputStream(new File(excelFajl));
        HSSFWorkbook workbook = new HSSFWorkbook(file);
        HSSFSheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.iterator();

        //az első sorra nincs szükségünk, mert az a fejléc
        for (int j = 0; j < 1; j++) {
            rowIterator.next();
        }

        int i = 0;
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            String neve = row.getCell(0).getStringCellValue();
            String gepnev = row.getCell(1).getStringCellValue();
            Date licKezdet = row.getCell(2).getDateCellValue();
            int licIdotartam = (int) row.getCell(3).getNumericCellValue();

            addLicence(new ProgramLicence(neve, gepnev, licKezdet, licIdotartam));
        }
    }

    public void save(String fajlnev) throws IOException {
        ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(fajlnev));
        out.writeObject(this);
        out.close();
    }

    public void save() throws IOException {
        if (fajlnev != null) {
            save(fajlnev);
        }
    }

    public void open(String fajlnev) throws FileNotFoundException, IOException, ClassNotFoundException {
        ObjectInputStream in = new ObjectInputStream(new FileInputStream(fajlnev));
        Licencek l = (Licencek) in.readObject();
        licenceTomb = l.licenceTomb;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (ProgramLicence licence : licenceTomb) {
            sb.append(licence).append("\n");
        }
        return sb.toString();
    }
}
