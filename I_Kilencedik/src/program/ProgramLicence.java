package program;

import java.io.Serializable;
import java.util.Date;

public class ProgramLicence implements Serializable {

    private String neve;
    private String gepnev;
    private Date licKezdet;
    private int licIdotartam = 0;

    public ProgramLicence(String neve, String gepnev, Date licKezdet, int licIdotartam) {
        this.neve = neve;
        this.gepnev = gepnev;
        this.licKezdet = licKezdet;
        this.licIdotartam = licIdotartam;
    }

    public boolean lejartE() {
        Date most = new Date();
        Date lejarat = new Date(licKezdet.getTime());
        lejarat.setYear(licKezdet.getYear() + 1);
        return lejarat.before(most);
    }

    @Override
    public String toString() {
        return "ProgramLicence{" + "neve=" + neve + ", gepnev=" + gepnev + ", licKezdet=" + licKezdet + ", licIdotartam=" + licIdotartam + '}';
    }

}
