package a_elso;

import java.util.Scanner;

public class G {
    
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Adjon meg egy számot: ");
        int a = in.nextInt();
        System.out.print("Adjon meg egy másik számot: ");
        int b = in.nextInt();
        in.close();
        
        System.out.println("A két szám összege: " + (a + b));
    }
    
}
