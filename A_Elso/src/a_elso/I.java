package a_elso;

import java.util.Scanner;

public class I {
    
    public static void main(String[] args) {
        double a, b, c, s, t;
        Scanner in = new Scanner(System.in);
        
        System.out.println("Első oldal: ");
        a = in.nextDouble();
        System.out.println("Második oldal: ");
        b = in.nextDouble();
        System.out.println("Harmadik oldal: ");
        c = in.nextDouble();
        
        s = (a + b + c) / 2;
        t = Math.sqrt(s * (s - a) * (s - b) * (s - c));
        
        System.out.println("A háromszög területe: " + t);
        
    }
}
