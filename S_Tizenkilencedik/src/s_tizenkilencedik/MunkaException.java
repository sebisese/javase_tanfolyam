package s_tizenkilencedik;

public class MunkaException extends Exception {

    public MunkaException(String message) {
        super(message);
    }

}
