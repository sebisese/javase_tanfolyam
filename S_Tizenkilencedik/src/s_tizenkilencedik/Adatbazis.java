package s_tizenkilencedik;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class Adatbazis {

    private String dbFajl;

    public Adatbazis() {
        dbFajl = "res/kerteszet.db";
        if (!isTableExists("TMunka")) {
            createMunka();
        }
    }

    public void saveOrUpdateMunka(Munka munka) {
        if (munka.getAzonosito() == null) {
            insertMunka(munka);
        } else {
            updateMunka(munka);
        }
    }

    public void insertMunka(Munka munka) {
        String sql = "INSERT INTO TMunka VALUES (NULL,?,?,?,?,?)";
        Connection c = null;
        PreparedStatement s = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + dbFajl);
            s = c.prepareStatement(sql);
            s.setString(1, munka.getNev());
            s.setString(2, munka.getE_mail());
            s.setString(3, munka.getKezdet().toString());
            s.setString(4, munka.getVeg().toString());
            s.setInt(5, munka.getAr());
            s.executeUpdate();
            s.close();
            System.out.println("Az insert lefutott.");
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public void updateMunka(Munka munka) {
        String sql = "UPDATE TMunka SET "
                + "nev=?, "
                + "e_mail=?, "
                + "kezdet=?, "
                + "veg=?, "
                + "ar=? "
                + "WHERE azonosito=?";
        Connection c = null;
        PreparedStatement s = null;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + dbFajl);
            s = c.prepareStatement(sql);
            s.setString(1, munka.getNev());
            s.setString(2, munka.getE_mail());
            s.setString(3, munka.getKezdet().toString());
            s.setString(4, munka.getVeg().toString());
            s.setInt(5, munka.getAr());
            s.setInt(6, munka.getAzonosito());
            s.executeUpdate();
            s.close();
            System.out.println("Az update lefutott.");
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public List<Munka> selectMunkak() {
        Connection c = null;
        Statement s = null;
        String sql = "SELECT * FROM TMunka ORDER BY kezdet ASC";
        List<Munka> result = new ArrayList<>();

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + dbFajl);
            s = c.createStatement();
            ResultSet r = s.executeQuery(sql);
            while (r.next()) {
                Munka munka = new Munka();
                munka.setAzonosito(r.getInt("azonosito"));
                munka.setNev(r.getString("nev"));
                munka.setE_mail(r.getString("e_mail"));
                munka.setKezdet(Date.valueOf(r.getString("kezdet")));
                munka.setVeg(Date.valueOf(r.getString("veg")));
                munka.setAr(r.getInt("ar"));
                result.add(munka);
            }
            s.close();
            System.out.println("A munkák lekérdezve.");
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return result;
    }

    public boolean isTableExists(String tablanev) {
        Connection c = null;
        boolean result = false;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + dbFajl);
            DatabaseMetaData dbm = c.getMetaData();
            ResultSet tables = dbm.getTables(null, null, tablanev, null);
            if (tables.next()) {
                result = true;
                System.out.println(tablanev + " tábla létezik. Hurrá!");
            } else {
                System.err.println(tablanev + " tábla nem létezik. Jajj! DE no para, létrehozom...");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return result;
    }

    private void createMunka() {
        String sql = "CREATE TABLE TMunka ("
                + "	azonosito INTEGER PRIMARY KEY, "
                + "	nev TEXT NOT NULL, "
                + "	e_mail TEXT NOT NULL, "
                + "	kezdet DATE NOT NULL, "
                + "	veg DATE NOT NULL, "
                + "	ar INTEGER NOT NULL "
                + ")";
        Connection c = null;
        Statement s = null;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + dbFajl);
            s = c.createStatement();
            s.executeUpdate(sql);
            s.close();
            System.out.println("A 'TMunka' táblát létrehoztam.");
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public void deleteMunka(Munka munka) {
        String sql = "DELETE FROM TMunka WHERE azonosito=?";
        Connection c = null;
        PreparedStatement s = null;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + dbFajl);
            s = c.prepareStatement(sql);
            s.setInt(1, munka.getAzonosito());
            System.out.println(s.toString());
//            s.executeUpdate();
//            s.close();
            System.out.println("A törlés lefutott.");
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                }
            }
        }
    }

}
