package s_tizenkilencedik;

import java.sql.Date;

public class Munka {

    private Integer azonosito;
    private String nev;
    private String e_mail;
    private Date kezdet;
    private Date veg;
    private Integer ar;

    public Munka() {
    }

    public Munka(Integer azonosito, String nev, String e_mail, Date kezdet, Date veg, Integer ar) {
        this.azonosito = azonosito;
        this.nev = nev;
        this.e_mail = e_mail;
        this.kezdet = kezdet;
        this.veg = veg;
        this.ar = ar;
    }

    public Integer getAzonosito() {
        return azonosito;
    }

    public void setAzonosito(Integer azonosito) {
        this.azonosito = azonosito;
    }

    public String getNev() {
        return nev;
    }

    public void setNev(String nev) {
        this.nev = nev;
    }

    public String getE_mail() {
        return e_mail;
    }

    public void setE_mail(String e_mail) {
        this.e_mail = e_mail;
    }

    public Date getKezdet() {
        return kezdet;
    }

    public void setKezdet(Date kezdet) {
        this.kezdet = kezdet;
    }

    public Date getVeg() {
        return veg;
    }

    public void setVeg(Date veg) {
        this.veg = veg;
    }

    public Integer getAr() {
        return ar;
    }

    public void setAr(Integer ar) {
        this.ar = ar;
    }

    @Override
    public String toString() {
        return kezdet.toString() + " - " + veg.toString();
    }

}
