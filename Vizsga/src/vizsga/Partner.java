package vizsga;

import java.sql.Date;

public class Partner {

    private Integer azonosito;
    private String nev;
    private Date szuletesiIdo;
    private Integer telefonszam;
    private String e_mail;

    public Partner() {
    }

    public Partner(Integer azonosito, String nev, Date szuletesiIdo, Integer telefonszam, String e_mail) {
        this.azonosito = azonosito;
        this.nev = nev;
        this.szuletesiIdo = szuletesiIdo;
        this.telefonszam = telefonszam;
        this.e_mail = e_mail;
    }

    public void setAzonosito(Integer azonosito) {
        this.azonosito = azonosito;
    }

    public Integer getAzonosito() {
        return azonosito;
    }

    public String getNev() {
        return nev;
    }

    public void setNev(String nev) {
        this.nev = nev;
    }

    public Date getSzuletesiIdo() {
        return szuletesiIdo;
    }

    public void setSzuletesiIdo(Date szuletesiIdo) {
        this.szuletesiIdo = szuletesiIdo;
    }

    public Integer getTelefonszam() {
        return telefonszam;
    }

    public void setTelefonszam(Integer telefonszam) {
        this.telefonszam = telefonszam;
    }

    public String getE_mail() {
        return e_mail;
    }

    public void setE_mail(String e_mail) {
        this.e_mail = e_mail;
    }

    @Override
    public String toString() {
        return nev;
    }

}
