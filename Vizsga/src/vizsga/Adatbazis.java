package vizsga;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class Adatbazis {

    private String dbFajl;
    
    public Adatbazis() {
        dbFajl = "res/partnerek.db";
        if (!isTableExists("TPartner")) {
            createTable();
        }
    }

    public void saveOrUpdate(Partner partner) {
        if (partner.getAzonosito() == null) {
            insert(partner);
        } else {
            update(partner);
        }
    }

    public void insert(Partner partner) {
        String sql = "INSERT INTO TPartner VALUES (NULL,?,?,?,?)";
        Connection c = null;
        PreparedStatement s = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + dbFajl);
            s = c.prepareStatement(sql);
            s.setString(1, partner.getNev());
            s.setString(2, partner.getSzuletesiIdo().toString());
            s.setInt(3, partner.getTelefonszam());
            s.setString(4, partner.getE_mail());
            s.executeUpdate();
            s.close();
            System.out.println("Az insert lefutott.");
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public void update(Partner partner) {
        String sql = "UPDATE TPartner SET "
                + "nev=?, "
                + "szuletesiIdo=?, "
                + "telefonszam=?, "
                + "e_mail=? "
                + "WHERE azonosito=?";
        Connection c = null;
        PreparedStatement s = null;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + dbFajl);
            s = c.prepareStatement(sql);
            s.setString(1, partner.getNev());
            s.setString(2, partner.getSzuletesiIdo().toString());
            s.setInt(3, partner.getTelefonszam());
            s.setString(4, partner.getE_mail());
            s.setInt(5, partner.getAzonosito());
            s.executeUpdate();
            s.close();
            System.out.println("Az update lefutott.");
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public List<Partner> select() {
        Connection c = null;
        Statement s = null;
        String sql = "SELECT * FROM TPartner ORDER BY nev ASC";
        List<Partner> result = new ArrayList<>();

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + dbFajl);
            s = c.createStatement();
            ResultSet r = s.executeQuery(sql);
            while (r.next()) {
                Partner partner = new Partner();
                partner.setAzonosito(r.getInt("azonosito"));
                partner.setNev(r.getString("nev"));
                partner.setSzuletesiIdo(Date.valueOf(r.getString("szuletesiIdo")));
                partner.setTelefonszam(r.getInt("telefonszam"));
                partner.setE_mail(r.getString("e_mail"));
                result.add(partner);
            }
            s.close();
            System.out.println("A partnereket lekérdeztem.");
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return result;
    }

    public boolean isTableExists(String tablanev) {
        Connection c = null;
        boolean result = false;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + dbFajl);
            DatabaseMetaData dbm = c.getMetaData();
            ResultSet tables = dbm.getTables(null, null, tablanev, null);
            if (tables.next()) {
                result = true;
                System.out.println(tablanev + " tábla létezik. Hurrá!");
            } else {
                System.err.println(tablanev + " tábla nem létezik. Jajj! DE no para, létrehozom...");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return result;
    }

    private void createTable() {
        String sql = "CREATE TABLE TPartner ("
                + "	azonosito INTEGER PRIMARY KEY, "
                + "	nev TEXT NOT NULL, "
                + "	szuletesiIdo DATE NOT NULL, "
                + "	telefonszam INTEGER NOT NULL, "
                + "	e_mail TEXT NOT NULL "
                + ")";
        Connection c = null;
        Statement s = null;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + dbFajl);
            s = c.createStatement();
            s.executeUpdate(sql);
            s.close();
            System.out.println("A 'TPartner' táblát létrehoztam.");
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public void delete(Partner partner) {
        String sql = "DELETE FROM TPartner WHERE azonosito=?";
        Connection c = null;
        PreparedStatement s = null;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + dbFajl);
            s = c.prepareStatement(sql);
            s.setInt(1, partner.getAzonosito());
            s.executeUpdate();
            s.close();
            System.out.println("A törlés lefutott.");
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                }
            }
        }
    }
    
}
