package vizsga;

public class PartnerException extends Exception {

    public PartnerException(String message) {
        super(message);
    }

}
