package m_tizenharmadik;

public class Diak {

    Integer magassag;
    String nev;

    public Diak(Integer magassag, String nev) {
        this.magassag = magassag;
        this.nev = nev;
    }

    @Override
    public String toString() {
        return nev;
    }

    public Integer getMagassag() {
        return magassag;
    }

    public String getNev() {
        return nev;
    }

}
