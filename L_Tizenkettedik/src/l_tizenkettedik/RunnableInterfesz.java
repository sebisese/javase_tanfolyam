package l_tizenkettedik;

import java.util.logging.Level;
import java.util.logging.Logger;

class RunnableDemo implements Runnable {

    private Thread t;
    private String nev;

    public RunnableDemo(String nev) {
        this.nev = nev;
        System.out.println("Létrehozva: " + nev);
    }

    @Override
    public void run() {
        System.out.println("Elindult a futás: " + nev);
        for (int i = 4; i > 0; i--) {
            try {
                System.out.println("A " + nev + " számlálója: " + i);
                Thread.sleep(50);
            } catch (InterruptedException ex) {
                Logger.getLogger(RunnableDemo.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void start() {
        System.out.println("Elindult: " + nev);
        if (t == null) {
            t = new Thread(this, nev);
            t.start();
        }
    }
}

public class RunnableInterfesz {

    public static void main(String[] args) {
        RunnableDemo r1 = new RunnableDemo("1. szál");
        r1.start();
        
        RunnableDemo r2 = new RunnableDemo("2. szál");
        r2.start();
        
        System.out.println("Main vége.");
    }
}
