package l_tizenkettedik;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.LinkedList;
import javax.swing.JApplet;

public class Haromszog extends JApplet implements MouseListener {

    LinkedList<Point> pontok;

    @Override
    public void paint(Graphics g) {
        g.clearRect(0, 0, getWidth(), getHeight());
        if (pontok.size() >= 2) {
            for (int i = 0; i < pontok.size(); i++) {
                g.drawLine(pontok.get(i).x, pontok.get(i).y,
                        pontok.get((i + 1) % pontok.size()).x, pontok.get((i + 1) % pontok.size()).y);
            }
        }
    }

    @Override
    public void init() {
        if (pontok.size() == 3) {
            pontok.poll();
        }
        addMouseListener(this);
        pontok = new LinkedList<>();
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        pontok.add(e.getPoint());
        repaint();
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

}
