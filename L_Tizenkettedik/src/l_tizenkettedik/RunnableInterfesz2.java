package l_tizenkettedik;

import java.util.logging.Level;
import java.util.logging.Logger;

class Futtathato implements Runnable {

    @Override
    public void run() {
        for (int i = 0; i < 20; i++) {
            System.out.println(Thread.currentThread().getName()
                    + " - " + i);
            try {
                Thread.sleep(20);
            } catch (InterruptedException ex) {
            }
        }
    }
}

public class RunnableInterfesz2 {

    public static void main(String[] args) {
        try {
            Futtathato fu = new Futtathato();
            Thread t1 = new Thread(fu, "Elso");
            Thread t2 = new Thread(fu, "Masodik");
            
            t1.start();
            t1.join();
            
            t2.start();
            t2.join();
            
            System.out.println("Main vége.");
        } catch (InterruptedException ex) {
            Logger.getLogger(RunnableInterfesz2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
