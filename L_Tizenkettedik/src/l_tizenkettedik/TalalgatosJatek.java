package l_tizenkettedik;

import java.math.MathContext;
import java.util.Random;

class UzenetMegjelenit implements Runnable {

    private String uzenet;

    public UzenetMegjelenit(String uzenet) {
        this.uzenet = uzenet;
    }

    @Override
    public void run() {
        while (true) {
            System.out.println(uzenet);
        }
    }
}

class GondoltamEgySzamra extends Thread {

    private int szam;

    public GondoltamEgySzamra(int szam) {
        this.szam = szam;
    }

    @Override
    public void run() {
        int szamlalo = 0;
        int tipp;
        do {
            tipp = (int) (Math.random() * 100 + 1);
            System.out.println(this.getName() + " - a tipp: " + tipp);
            szamlalo++;
        } while (tipp != szam);
        System.out.println("Talált!!! A szám: " + tipp + ". Próbálkozások száma: " + szamlalo);
    }
}

public class TalalgatosJatek {

    public static void main(String[] args) {
        
        Thread koszon1 = new Thread(new UzenetMegjelenit("Helló"));
        koszon1.setDaemon(true);
        koszon1.setName("Helló thread.");
        System.out.println("Helló elindult...");
        koszon1.start();
        
        Thread jatek1 = new GondoltamEgySzamra(21);
        jatek1.setName("jatek1 thread");
        System.out.println("jatek1 elindult...");
        jatek1.start();
        
        System.out.println("Main vége.");
    }
}
