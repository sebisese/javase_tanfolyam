package exception;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Eletkor {

    public static void main(String[] args) {
        try {
            int kor = eletkorBeker();
            System.out.println(kor);
        } catch (RosszEletkorException ex) {
            System.out.println(ex.getMessage());
        } catch (InputMismatchException ix) {
            System.out.println("Nem egész számot írt be.");
        }
    }

    private static int eletkorBeker() throws RosszEletkorException {
        Scanner in = new Scanner(System.in);
        System.out.print("Adja meg az életkorát: ");
        int szam = in.nextInt();
        if (szam < 0 || szam > 150) {
            throw new RosszEletkorException();
        }
        return szam;
    }

}
