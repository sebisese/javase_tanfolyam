package exception;

public class RosszEletkorException extends Exception {

    public RosszEletkorException() {
        super("Rossz életkort írt be.");
    }

}
