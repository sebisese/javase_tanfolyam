package f_hatodik;

public class AutoFelhasznalas {

    public static void main(String[] args) {
        Auto2 a;
        a = new Auto2("ABC-123", 55, 40, 8);
        System.out.println(a.hanyLitertFogFogyasztani(40));

        Auto2 a2 = new Auto2("DEF-456", 55, 40, 8);
        System.out.println(a2.mennyibeKerult(40));
        Auto2.benzinarValtozas(300);
        System.out.println(a2.mennyibeKerult(40));

        Teherauto teher = new Teherauto("TTT-100", 300, 200, 19, 2);
        Auto2 teher2 = new Teherauto("TTT-101", 300, 160, 19, 2);

        System.out.println(teher.mennyibeKerult(80));
        System.out.println(teher2.mennyibeKerult(80));

        System.out.println(teher.getMennyiSulytVihet());
        System.out.println(((Teherauto) teher2).getMennyiSulytVihet());
        
//        Integer szam = 12;
//        Object oszam = szam;
//        Object o = teher;
//        System.out.println(o.toString());
    }

}
