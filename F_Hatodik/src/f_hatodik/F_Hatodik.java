package f_hatodik;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class F_Hatodik {

    public static void main(String[] args) {

        try {
            
            //1. feladat
            Helyezes[] helyezesek = beolvas("helyezes.csv");

            //2. feladat
            System.out.println("Csapatok:");
            for (String csapat : getCsapatok(helyezesek)) {
                System.out.println(" - " + csapat);
            }

            //3. feladat
            System.out.println("---");
            for (Helyezes helyezes : helyezesek) {
                System.out.println(helyezes);
            }
            
            //4. feladat
            //Kik versenyeznek abban az évben?
            
            //5. feladat
            //Számolj mindenkihez aktuális állást!
            
            //6. feladat
            //Hogy állnak a versenyzők? (Renezd pontszám szerint!)
            
        } catch (IOException ex) {
            System.out.println("Nem létezik a helyezes.csv fájl.");
        }

    }

    private static String[] getCsapatok(Helyezes[] helyezesek) {
        String[] csapatok = new String[helyezesek.length];
        int db = 0;
        for (int i = 0; i < helyezesek.length; i++) {
            if (!benneVanE(helyezesek[i].getCsapat(), csapatok, db)) {
                csapatok[db] = helyezesek[i].getCsapat();
                db++;
            }
        }
        return java.util.Arrays.copyOf(csapatok, db);
    }

    private static boolean benneVanE(String csapat, String[] csapatok, int db) {
        int i = 0;
        while (i < db && !(csapat.equals(csapatok[i]))) {
            i++;
        }
        return i < db;
    }

    private static Helyezes[] beolvas(String fajl) throws IOException {
        String egeszSzoveg = new String(Files.readAllBytes(Paths.get(fajl)));
        String[] sorok = egeszSzoveg.split("\r\n");
        Helyezes[] helyezesek = new Helyezes[sorok.length];
        for (int i = 0; i < sorok.length; i++) {
            String[] tmp = sorok[i].split(" ");
            helyezesek[i] = new Helyezes(tmp[0], tmp[1], tmp[2], Integer.parseInt(tmp[3]));
        }
        return helyezesek;
    }
}
