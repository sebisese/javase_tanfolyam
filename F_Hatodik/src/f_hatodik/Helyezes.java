package f_hatodik;

public class Helyezes {

    //static final ->konstans, csak nagybetűvel
    private static final int[] PONTOK = {25, 18, 15, 12, 10, 8, 6, 4, 2, 1};
    private String csapat;
    private String versenyzo;
    private String palya;
    private int helyezes;

    public Helyezes(String csapat, String versenyzo, String palya, int helyezes) {
        this.csapat = csapat;
        this.versenyzo = versenyzo;
        this.palya = palya;
        this.helyezes = helyezes;
    }

    public String getCsapat() {
        return csapat;
    }

    public String getVersenyzo() {
        return versenyzo;
    }

    public String getPalya() {
        return palya;
    }

    public int getHelyezes() {
        return helyezes;
    }

    public int getPont() {
        return PONTOK[helyezes - 1];
    }

    @Override
    public String toString() {
//        String s = "";
//        s += "Versenyző: " + versenyzo + "\n";
//        s += "Csapat: " + csapat + "\n";
//        s += "Pálya: " + palya + "\n";
//        s += "Helyezés: " + helyezes + "\n";
//        s += "Pont: " + getPont();
        return versenyzo + " (" + csapat + ") \t " + palya + " \t " + getPont() + " pont (" + helyezes + ". hely)";
    }
}
