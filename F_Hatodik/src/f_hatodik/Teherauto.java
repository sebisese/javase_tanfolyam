package f_hatodik;

public class Teherauto extends Auto2{

    private int mennyiSulytVihet;
    
    public Teherauto(String rendszam, int tartalyKapacitas, double tartalySzint, 
            double fogyasztas, int mennyiSulytVihet) {
        super(rendszam, tartalyKapacitas, tartalySzint, fogyasztas);
        this.mennyiSulytVihet = mennyiSulytVihet;
    }

    public int getMennyiSulytVihet() {
        return mennyiSulytVihet;
    }
    
    
}
