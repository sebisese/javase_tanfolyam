package level;

import java.io.Serializable;

public class Level implements Serializable {

    private Adat kuldo;
    private Adat cimzett;
    private String uzenet;

    public Adat getKuldo() {
        return kuldo;
    }

    public Adat getCimzett() {
        return cimzett;
    }

    public String getUzenet() {
        return uzenet;
    }

    public void setKuldo(Adat kuldo) {
        this.kuldo = kuldo;
    }

    public void setCimzett(Adat cimzett) {
        this.cimzett = cimzett;
    }

    public void setUzenet(String uzenet) {
        this.uzenet = uzenet;
    }

    @Override
    public String toString() {
        return "Level{" + "kuldo=" + kuldo + "\ncimzett=" + cimzett + "\nuzenet=" + uzenet + '}';
    }

}
