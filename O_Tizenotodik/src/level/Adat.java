package level;

import java.io.Serializable;

public class Adat implements Serializable {

    private String nev;
    private String cim;
    private String varos;
    private String iranyitoszam;

    public Adat(String nev, String cim, String varos, String iranyitoszam) {
        this.nev = nev;
        this.cim = cim;
        this.varos = varos;
        this.iranyitoszam = iranyitoszam;
    }

    //ha van paraméterezett konstruktor, akkor kell egy "üres" default is
    //ha nincs paraméterezett, akkor van default konstruktor
    public Adat() {
    }

    public String getNev() {
        return nev;
    }

    public String getCim() {
        return cim;
    }

    public String getVaros() {
        return varos;
    }

    public String getIranyitoszam() {
        return iranyitoszam;
    }

    public void setNev(String nev) {
        this.nev = nev;
    }

    public void setCim(String cim) {
        this.cim = cim;
    }

    public void setVaros(String varos) {
        this.varos = varos;
    }

    public void setIranyitoszam(String iranyitoszam) {
        this.iranyitoszam = iranyitoszam;
    }

    @Override
    public String toString() {
        return "Adat{" + "nev=" + nev + ", cim=" + cim + ", varos=" + varos + ", iranyitoszam=" + iranyitoszam + '}';
    }

}
