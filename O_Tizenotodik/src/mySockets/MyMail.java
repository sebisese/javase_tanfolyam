package mySockets;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class MyMail {

    public static void main(String[] args) {

        String kitol = "obama@usa.gov.us";
        String kinek = "balazs85@gmail.com";
        String szoveg = "Hello!";
        
        Properties beallitasok = System.getProperties();
        beallitasok.setProperty("mail.smtp.host", "smtp.upcmail.hu");
        Session session = Session.getDefaultInstance(beallitasok);

        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(kitol));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(kinek));
            message.setSubject("Ezt mindenképp olvasd el!");
            message.setText(szoveg);
            Transport.send(message);
            System.out.println("Sikerült elküldeni");
        } catch (MessagingException messagingException) {
            System.out.println(messagingException.getMessage());
        }

    }

}
