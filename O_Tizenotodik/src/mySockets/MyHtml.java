package mySockets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

public class MyHtml {

    public static void main(String[] args) throws IOException {
        String cim = "www.elte.hu";
        int port = 80;
        Socket s = new Socket(cim, port);
        InputStream is = s.getInputStream();
        OutputStream os = s.getOutputStream();
        PrintWriter out = new PrintWriter(new OutputStreamWriter(os));
        BufferedReader in = new BufferedReader(
                new InputStreamReader(is));
        out.println("GET / HTTP/1.0");
        out.println("Accept:text/html");
        out.println("Host: " + cim);
        out.println();
        out.flush();
        String line;
        while ((line = in.readLine()) != null) {
            System.out.println(line);
        }
        in.close();
        out.close();
        s.close();
    }

}
