package applet;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.JApplet;

public class Teglalaprajzolo extends JApplet implements MouseListener, MouseMotionListener {

    Point kezdoPont;
    Point vegPont;

    @Override
    public void init() {
        addMouseListener(this);
        addMouseMotionListener(this);
    }

    @Override
    public void paint(Graphics g) {
        g.clearRect(0, 0, getWidth(), getHeight());
        if (kezdoPont != null && vegPont != null) {
            int x = Math.min(kezdoPont.x, vegPont.x);
            int y = Math.min(kezdoPont.y, vegPont.y);
            int w = Math.abs(vegPont.x - kezdoPont.x);
            int h = Math.abs(vegPont.y - kezdoPont.y);
            g.drawRect(x, y, w, h);
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        kezdoPont = e.getPoint();
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        vegPont = e.getPoint();
        repaint();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mouseMoved(MouseEvent e) {
    }

}
