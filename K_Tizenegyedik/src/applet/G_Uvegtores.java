package applet;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.ImageIcon;
import javax.swing.JApplet;

public class G_Uvegtores extends JApplet implements MouseListener {

    Point aktualis;
    Image kep;

    @Override
    public void init() {
        addMouseListener(this);
        kep = new ImageIcon("res/brokenKis2.png").getImage();
    }

    @Override
    public void paint(Graphics g) {
        if (aktualis != null) {
            g.drawImage(kep, aktualis.x, aktualis.y, null);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        aktualis = new Point(e.getX() - 26, e.getY() - 23);
        repaint();
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }
}
