package applet;

import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import javax.swing.JApplet;

public class D_Esemeny extends JApplet implements MouseListener {

    ArrayList<String> esemenyek;

    @Override
    public void init() {
        addMouseListener(this);
        esemenyek = new ArrayList<>();
        esemenyTortent("Inicializálás lefutott.");
    }

    public void esemenyTortent(String esemeny) {
        esemenyek.add(esemeny);
        System.out.println(esemeny);
        repaint();
    }

    @Override
    public void paint(Graphics g) {
        for (int i = 0; i < esemenyek.size(); i++) {
            g.drawString(esemenyek.get(i), 10, 20 + i * 15);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        esemenyTortent("egérKattintás(" + e.getX() + "," + e.getY() + ")");
    }

    @Override
    public void mousePressed(MouseEvent e) {
        esemenyTortent("gombLenyomás(" + e.getX() + "," + e.getY() + ")");
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        esemenyTortent("gombFelengedés(" + e.getX() + "," + e.getY() + ")");
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        esemenyTortent("belépésARajzterületr(" + e.getX() + "," + e.getY() + ")");
    }

    @Override
    public void mouseExited(MouseEvent e) {
        esemenyTortent("kilépésARajzterületről(" + e.getX() + "," + e.getY() + ")");
    }
}
