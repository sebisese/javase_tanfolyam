package applet;

import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JApplet;
import javax.swing.JPanel;

public class J_Billentyuzet extends JApplet implements KeyListener {

    JPanel panel;
    String text = "";
    
    @Override
    public void init() {
        addKeyListener(this);
        setFocusable(true);
    }
    
    @Override
    public void paint(Graphics g) {
        if (text != null) {
            g.clearRect(0, 0, getWidth(), getHeight());
            g.drawString(text, 0, 20);
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
        System.out.println(e.getKeyCode());
    }
    
    @Override
    public void keyTyped(KeyEvent e) {
        System.out.println(e.getKeyChar());
        text += e.getKeyChar();
        repaint();
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }
    
}
