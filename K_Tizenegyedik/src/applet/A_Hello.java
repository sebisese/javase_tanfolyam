package applet;

import java.awt.Graphics;
import javax.swing.JApplet;

public class A_Hello extends JApplet {

    @Override
    public void paint(Graphics g) {
        g.drawString("Hello World", 25, 50);
    }
}
