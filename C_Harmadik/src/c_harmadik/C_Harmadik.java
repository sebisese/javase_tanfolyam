package c_harmadik;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class C_Harmadik {

    static String[] termek;
    static int[] ar;
    static int[] db;

    public static void main(String[] args) throws IOException {

        beolvas("bevlista.csv");

        kiir();

        System.out.println("--------------------");
        System.out.println("SUM:\t" + vegosszeg());
    }

    private static void beolvas(String fajl) throws IOException {
        String egeszSzoveg = new String(Files.readAllBytes(Paths.get(fajl)));
        String[] sorok = egeszSzoveg.split("\r\n");

        termek = new String[sorok.length];
        ar = new int[sorok.length];
        db = new int[sorok.length];

        for (int i = 0; i < sorok.length; i++) {
            String[] oszlopok = sorok[i].split(";");
            termek[i] = oszlopok[0];
            ar[i] = Integer.parseInt(oszlopok[1]);
            db[i] = Integer.parseInt(oszlopok[2]);
        }
    }

    private static void kiir() {
        for (int i = 0; i < termek.length; i++) {
            System.out.println(termek[i] + ":\t" + (ar[i] * db[i]));
        }
    }

    private static int vegosszeg() {
        int sum = 0;
        for (int i = 0; i < ar.length; i++) {
            sum += ar[i] * db[i];
        }
        return sum;
    }

}
