package c_harmadik;

public class OsszegFuggveny {
    
    public static void main(String[] args) {
        int a = 3;
        int b = 7;
        int c = osszeg(a, b);
        System.out.println(c);
    }
    
    private static int osszeg(int egyik, int masik) {
        return egyik + masik;
    }
}
