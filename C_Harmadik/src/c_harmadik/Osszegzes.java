package c_harmadik;

public class Osszegzes {

    public static void main(String[] args) {

        double[] hom = {19, 17, 19, 20, 19, 22, 20};
        System.out.println("A heti átlag hőmérséklet: " + atlag(hom));
        if (fagyottE(hom)) {
            System.out.println("Fagyott a héten");
        } else {
            System.out.println("Nem fagyott a héten.");
        }
        System.out.println("A legnagyobb hőmérséklet: " + hom[maxHomerseklet(hom)]);        
        System.out.println("Hányszor volt 19°C a héten? " + megszamlalas(hom));

    }

    private static double atlag(double[] hom) {
        return osszeg(hom) / hom.length;
    }

    private static double osszeg(double[] hom) {
        int sum = 0;
        for (double i : hom) {
            sum += i;
        }
        return sum;
    }

    private static boolean fagyottE(double[] hom) {
        int i = 0;
        while (i < hom.length && hom[i] >= 0) {
            i++;
        }
        return i < hom.length;
    }

    private static int maxHomerseklet(double[] hom) {
        int max = 0;
        for (int i = 1; i < hom.length; i++) {
            if (hom[i] > hom[max]) {
                max = i;
            }
        }
        return max;
    }

    private static int megszamlalas(double[] hom) {
        int db = 0;
        for (int i = 0; i < hom.length; i++) {
            if (hom[i] == 19) {
                db++;
            }
        }
        return db;
    }
}
