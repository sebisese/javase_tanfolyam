package c_harmadik;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class BevasarlolistaKeszito {

    public static void main(String[] args) throws IOException {
        Scanner in = new Scanner(System.in);

        PrintWriter pw = new PrintWriter(new FileWriter("bevasarlolista.csv"));

//        do {
            System.out.print("Terméknév: ");
            String termek = in.next();
            System.out.print("Egységár: ");
            int ar = in.nextInt();
            System.out.print("Darabszám: ");
            int db = in.nextInt();
            pw.println(termek + ";" + ar + ";" + db);
            System.out.println(in.nextLine().toString());
//        } while (in.hasNext());

        pw.close();
    }

}
