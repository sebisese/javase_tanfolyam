package c_harmadik;

public class Fuggvenyek {

    public static void main(String[] args) {
        
        //Függvények / 2
        int szam = -11;
        if (parosE(szam)) {
            System.out.println(szam + " páros.");
        } else {
            System.out.println(szam + " páratlan.");
        }
        
        //Függvények / 3
        double[] tomb = {1, 5, 23, 7};
        System.out.println("A tömb elemeinek összege: " + osszeg(tomb));
        
        //Függvények / 4
        double[] vektor = {3, 4};
        System.out.println("Helyvektor hossza: " + helyvektorHossza(vektor));
        
        //Függvények / 5
//        int sz = 17;
//        if (primE(sz)) {
//            System.out.println("Prím.");
//        } else {
//            System.out.println("Nem prím.");
//        }
    }

    private static boolean parosE(int i) {
        return i % 2 == 0;
    }

    private static double osszeg(double[] tomb) {
        double sum = 0;
        for (double u : tomb) {
            sum += u;
        }
        return sum;
    }

    private static double helyvektorHossza(double[] vektor) {
        double hossz = 0;
        for (double w : vektor) {
            hossz += w*w;
//            hossz += Math.pow(w, 2);
        }
        return Math.sqrt(hossz);
    }

//    private static boolean primE(int sz) {
//        int i = 2;
//        while (i < sz && i ) {
//            
//        }
//    }
}
