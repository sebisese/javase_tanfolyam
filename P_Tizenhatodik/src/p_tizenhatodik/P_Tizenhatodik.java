package p_tizenhatodik;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.Scanner;

public class P_Tizenhatodik {

    private static String dbFajl = "res/test.db";

    public static void main(String[] args) {
//        kapcsolodas();
//        tablaKeszites();

//        String sql = "INSERT INTO TDolgozo VALUES (1, 'Senki Alfonz', 1990, 2000, 100000)";
//        updateFuttat(sql);
//        sql = "INSERT INTO TDolgozo VALUES (2, 'Szuper Áron', 2000, NULL, 120000)";
//        updateFuttat(sql);
//        sql = "INSERT INTO TDolgozo (nev, belepesEve, fizetes) VALUES ('Szalmon Ella', 1998, 120000) ";
//        updateFuttat(sql);
        String sql = "SELECT * FROM TDolgozo";
//        leker(sql);
//        leker2(sql);
        
//        kirug(2015, 2);
        
//        leker2(sql);
        
        Scanner in = new Scanner(System.in);
        String nev = in.nextLine();
        kirug2(nev);
        
//        String nev = "Senki Alfonz";
//        torol(nev);
//        leker2(sql);
    }

    private static void kapcsolodas() {
        Connection c = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + dbFajl);
            System.out.println("Kapcsolódtam.");
        } catch (ClassNotFoundException ex) {
            System.err.println(ex.getMessage());
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                }
            }
        }
    }

    private static void tablaKeszites() {
        String sql = "CREATE TABLE TDolgozo (\n"
                + "	azon INTEGER PRIMARY KEY, \n"
                + "	nev TEXT NOT NULL, \n"
                + "	belepesEve INTEGER NOT NULL, \n"
                + "	kilepesEve INTEGER, \n"
                + "	fizetes INTEGER NOT NULL\n"
                + ")";
        Connection c = null;
        Statement s = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + dbFajl);

            s = c.createStatement();
            s.executeUpdate(sql);

            s.close();
            System.out.println("Elkészítettem a táblát.");
        } catch (ClassNotFoundException ex) {
            System.err.println(ex.getMessage());
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                }
            }
        }
    }

    private static void updateFuttat(String sql) {
        Connection c = null;
        Statement s = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + dbFajl);

            s = c.createStatement();
            s.executeUpdate(sql);

            s.close();
            System.out.println("Lefuttattam: " + sql);
        } catch (ClassNotFoundException ex) {
            System.err.println(ex.getMessage());
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                }
            }
        }
    }

    private static void leker(String sql) {
        Connection c = null;
        Statement s = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + dbFajl);

            s = c.createStatement();
            ResultSet r = s.executeQuery(sql);

            while (r.next()) {
                String nev = r.getString("nev");
//                int belepesEve = r.getInt("belepesEve");
//                int kilepesEve = r.getInt("kilepesEve");
//                int fizetes = r.getInt("fizetes");
                System.out.println(r.getMetaData().getColumnTypeName(2));
                System.out.println(r.getMetaData().getColumnTypeName(1));
//                System.out.println(nev + ": " + belepesEve + "-" + kilepesEve + " (" + fizetes + ")");
                System.out.println(nev);
            }

            s.close();
            System.out.println("Lefuttattam: " + sql);
        } catch (ClassNotFoundException ex) {
            System.err.println(ex.getMessage());
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                }
            }
        }
    }

    private static void leker2(String sql) {
        Connection c = null;
        Statement s = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + dbFajl);

            s = c.createStatement();
            ResultSet r = s.executeQuery(sql);

            ResultSetMetaData rsmd = r.getMetaData();
            int oszlopokSzama = rsmd.getColumnCount();

            while (r.next()) {
                for (int i = 1; i <= oszlopokSzama; i++) {
//                    System.out.print(rsmd.getColumnName(i));
//                    System.out.print(": ");
                    System.out.println(r.getObject(i));
                }
//                System.out.println("---");
            }

            s.close();
            System.out.println("Lefuttattam: " + sql);
        } catch (ClassNotFoundException ex) {
            System.err.println(ex.getMessage());
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                }
            }
        }
    }

    private static void kirug(int ev, int azon) {
        String sql = "UPDATE TDolgozo SET kilepesEve=? WHERE azon=?";
        Connection c = null;
        PreparedStatement s = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + dbFajl);

            s = c.prepareStatement(sql);
            s.setInt(1, ev);
            s.setInt(2, azon);
            int modositottSorokSzama = s.executeUpdate();
            s.close();
            System.out.println("Kirúgtuk. (" + modositottSorokSzama + ")");
        } catch (ClassNotFoundException ex) {
            System.err.println(ex.getMessage());
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                }
            }
        }
    }

    private static void kirug2(String nev) {
        String sql = "UPDATE TDolgozo SET kilepesEve=? WHERE nev=?";
        Connection c = null;
        PreparedStatement s = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + dbFajl);

            s = c.prepareStatement(sql);
            s.setInt(1, Calendar.getInstance().get(Calendar.YEAR));
            s.setString(2, nev);
            int modositottSorokSzama = s.executeUpdate();
            s.close();
            if (modositottSorokSzama == 0) {
                System.out.println("Nincs ilyen felhasználó.");
                System.out.println("Válassz közülük: ");
                leker2("SELECT nev FROM TDolgozo");
            } else {
                System.out.println(nev + "-t kirúgtuk. (" + modositottSorokSzama + ")");
            }
        } catch (ClassNotFoundException ex) {
            System.err.println(ex.getMessage());
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                }
            }
        }
    }

}
